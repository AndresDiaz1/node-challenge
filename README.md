# Node.js Challenge!

## Information
The startup script runs [nodemon](https://www.npmjs.com/package/nodemon), so you won't need to keep stopping and reloading the server. The API that will be used to query the movie details will be [This one](https://yts.am/api).

## Requirements:
* Node.js 6.12.2
_(It is recommended this version due to the requirements of the project. If you need to switch to a newer version, try using [n](https://www.npmjs.com/package/n))._
* [ESLint](https://eslint.org/docs/user-guide/getting-started) _(Install it globally and install the plugin for your IDE).

## Setup
* Clone this repository
* Run `npm install`
* After the installation of the packages, run `npm start` and head to http://localhost:3000 to see the default template.

## Steps
1. Install [request-promise](https://www.npmjs.com/package/request-promise) and add it to the dependencies, so you can handle the requests.
2. Create a `/details` route for the movie details. Use the `movie-page-full.nunjucks` content part for the template.
3. Also create a `/search` route and template for the search results.
4. Create a `layout.nunjucks` Inside the `views/` folder that includes the common elements for the homepage and the movie details page, and extend it from those templates.
5. Split the different elements from each page (Carousel, movie lists, etc.) into different partials and include them from each template.
6. Once you have the different sections divided on templates and partials, add requests to the [API](https://yts.am/api) the following way:

  * Homepage:
    1. For the main stage, use the first 6 results of the [List Movies](https://yts.am/api/v2/list_movies.jsonp) endpoint. Show the poster of the movie, along with the rating by default, and the title, runtime, genres and year. The runtime must be shown in HH:mm format, as the example.
    2. For the **NOW IN THE CINEMA** section, use the same endpoint and show the next 8 results. It's important to use the `limit` query parameter when doing the request to the endpoint. Show the poster, as well as the Title, runtime in HH:mm format, genres and rating.
       * **Important:** These results MUST be shown interleaved as they're shown on the default template. It's up to you to handle this with CSS or within the nunjucks template.
    3. The search widget used here should look and behave just like the one on the movie details page, but placed where the current one is.
  * Movie details:
    1. The search functionality should work the same as the one on the homepage.
    2. Use the provided fields as shown in the example.
    3. Use the `background_images`, and `yt_trailer_code` fields **repeated a random number of times between 1 and 10 each one** to populate the Photos and videos slider, and update the media counts. All of these must show on the modal window.
    4. The Suggested movies section should be fetched from the [Movie suggestions](https://yts.am/api/v2/movie_suggestions.json?movie_id=10) endpoint, and rendered as it is.
  * Search results: 
    1. As there's no provided template, try to extrapolate some clear results from the provided templates and link them to the movie details page.
7. When these pages get finished, be sure that there's no Linting errors. Remember that you're free to use ES6, as Node.js is completely able to transpile it.

## Suggestions:
* Try to preserve the folder structure.
* Follow the API documentation at https://yts.am/api for the required parameters.
* Use the filters functionality from Handlebars to translate the time from minutes to HH:mm. If there's need to create a new filter, use the included `filters.js` file.

## Just in case you have the time:
* Translate the LESS stylesheets to SCSS and add compilation into the project.
* Add the same functionality from the homepage main stage to the Suggested movies section.
* Do something interesting with the banner on the homepage and the title bar.