const lessToScss = require('gulp-less-to-scss');
const gulp = require('gulp');
const sass = require('gulp-sass');

gulp.task('lessToScss', () => {
  gulp.src('public/css/less/**/*.less')
    .pipe(lessToScss())
    .pipe(gulp.dest('public/css/scss'));
});

gulp.task('sass', () => {
  gulp.src('public/css/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('public/css/style.css'));
});
