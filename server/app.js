const path = require('path');
const express = require('express');
const nunjucks = require('nunjucks');
const routes = require('../routes');
const filters = require('./filters');

const app = express();

// Setup nunjucks templating engine
const viewEngine = nunjucks.configure('views', {
  autoescape: true,
  watch: true,
  express: app,
});

/* add custom filters */
Object.keys(filters).forEach((name) => {
  viewEngine.addFilter(name, filters[name]);
});

app.set('port', process.env.PORT || 3001);

app.use(express.static(path.join(__dirname, '/../public')));
app.use(routes);

// Kick start our server
app.listen(app.get('port'), () => {
  console.log('Server started on port', app.get('port'));
});
