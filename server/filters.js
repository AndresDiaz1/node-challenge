module.exports = {
  dump: obj => `<pre> ${JSON.stringify(obj)} </pre>`,
  convertNumberToHours: (num) => {
    const hours = Math.floor(num / 60);
    const minutes = num % 60;
    return `${hours}h:${minutes}min`;
  }
  // A poor man's object dump functionality, ex: {{ context | dump | safe }}
};