const rp = require('request-promise');

const getRandomNUmber = () => Math.floor(Math.random() * 10) + 1;
const createSlidesArray = ((numberOfVideos, numberOfPhotos) => {
  const slidesArray = new Array(numberOfVideos + numberOfPhotos);
  slidesArray.fill('video', 0, numberOfVideos);
  slidesArray.fill('photo', numberOfVideos, (numberOfVideos + numberOfPhotos));
  return slidesArray.sort((a, b) => 0.5 - Math.random());
});

const detail = (req, res) => {
  const templateConfig = {
    page: 'detail/',
    port: process.env.PORT,
  };

  const movieId = req.params.id;

  const optionsSuggestionMovies = {
    uri: `https://yts.am/api/v2/movie_suggestions.json?movie_id=${movieId}`,
    json: true,
  };

  const optionsMovieDetail = {
    uri: `https://yts.am/api/v2/movie_details.json?movie_id=${movieId}`,
    json: true,
  };

  const suggestionMoviesPromise = rp(optionsSuggestionMovies);
  const movieDetailPromise = rp(optionsMovieDetail);

  Promise.all([suggestionMoviesPromise, movieDetailPromise]).then((response) => {
    templateConfig.suggestedMovies = response[0].data.movies;
    templateConfig.movieDetails = response[1].data.movie;
    templateConfig.trailerSlidesNumber = getRandomNUmber();
    templateConfig.BackgroundSlidesNumber = getRandomNUmber();
    templateConfig.slidesArray = createSlidesArray(templateConfig.trailerSlidesNumber, 
      templateConfig.BackgroundSlidesNumber);
    res.render('../views/templates/movie-page-full.nunjucks', templateConfig);
  }).catch((err) => {
    console.log('Error server', err);
    res.render('../views/templates/movie-page-full.nunjucks', templateConfig);
  });
};

module.exports = detail;
