const rp = require('request-promise');

const homepage = (req, res) => {
  const templateConfig = {
    page: 'home',
    port: process.env.PORT,
  };

  const options = {
    uri:'https://yts.am/api/v2/list_movies.json?limit=15',
    json: true,
  }

  rp(options).then((response) => {
    const nowInCinemaMovies = response.data.movies.slice(0,8)
    const todaysBestChoices = response.data.movies.slice(9,15)
    templateConfig.nowInCinemaMovies = nowInCinemaMovies;
    templateConfig.todaysBestChoices = todaysBestChoices;
    res.render('../views/templates/homepage.nunjucks', templateConfig);
  }).catch((err) => {
    console.log('Error server', err);
    res.render('../views/templates/homepage.nunjucks', templateConfig);
  });
};

module.exports = homepage;
