const router = require('express').Router();

router.get('/', require('./homepage'));
router.get('/details/:id', require('./detail'));
router.get('/search', require('./search'));

module.exports = router;
