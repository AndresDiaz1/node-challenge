const search = (req, res) => {
  const templateConfig = {
    page: 'search',
    port: process.env.PORT,
  };

  res.render('../views/templates/search.nunjucks', templateConfig);
};

module.exports = search;
